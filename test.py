import csv
import geocoder
import time

count = 0

##geocoders = [geocoder.arcgis, geocoder.google, geocoder.osm, geocoder.yahoo, geocoder.yandex]
geocoders = [geocoder.bing]
geocoder_count = 0;
bing_count = 0;
bing_keys = ['Al82-_fW4UHi_uIv4NumI-7gCqK-kXX8aczdyE9ol3ycYlGD3zHBzlvahJ7JRlyv', 'Ar4__vJaSt2vSTzOyXf0GZzw55tKl8eNuwBaZmcLkTWeWZmcSiFH9jEZcdVLRPN7', 'AjCFHml40ptsb6BdBzqkdYafJ37T1U9bdz1q8gS_6z7P2IGfpJS2qyxb6e_pNnjg', 'AoOFHCuV9JXR2trQUeFlDOL99gshSVK5p1aCCAucoBB5AjxoRF2ny2QCWhLzjVxu']
bing_key = bing_keys[0]

with open('brighton_complete.csv', 'wb') as outputcsvfile:
	fieldnames = ['address', 'postcode', 'price', 'date', 'latitude', 'longitude']
	outwriter = csv.DictWriter(outputcsvfile, delimiter=',', fieldnames=fieldnames)
	with open('stilltoprocess.csv', 'rb') as csvfile:
		brightonreader = csv.reader(csvfile, delimiter=",")
		for row in brightonreader:
			address = row[7:13]
			address.append(row[3])
			address = ' '.join(address)
			g = geocoder.bing(address, key=bing_key)
			latlong = g.latlng
			while (latlong == []):
				print "using geocoder..." + str(geocoder_count) + " api_key:" + str(bing_count);
				geocoder_count = (geocoder_count + 1) % len(geocoders)
				bing_count = (bing_count + 1) % len(bing_keys)
				bing_key = bing_keys[bing_count]
				g = geocoders[geocoder_count](address, key=bing_key)
				latlong = g.latlng
			loopcount = 0
			location = {}
			location["address"] = address
			location["postcode"] = row[3]
			location["price"] = row[1]
			location["date"] = row[2]
			location["latitude"] = latlong[0];
			location["longitude"] = latlong[1];
			outwriter.writerow(location)
			count = count + 1
			print count

## look into the requests package and how you can use that to get geocodes from the api matt suggested