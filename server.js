var express        = require('express');
var orm            = require('orm');
var swig           = require('swig');
var path           = require('path');
var bodyParser     = require('body-parser');
//var connect        = require('connect');
var methodOverride = require('method-override');
var controllers    = require('./controllers');
var fs             = require('fs');
var parse          = require('csv-parse');
var transform      = require('stream-transform');
//var async          = require('async');

//var geocoderProvider = 'google';
//var httpAdapter = 'http';
//var geocoder = require('node-geocoder')(geocoderProvider, httpAdapter);


var app = express();

//helper function to call res.format on a given route
function request_response(page) {
    return function (req, res) {
        res.format(page);
    };
}

//helper function to report an error when loading the database
function db_error_reporter(model) {
    return function (err) {
        if (err) {
            console.log("DB Error: Failed loading table " + model);
            console.log(err);
            console.log("Closing down server");
            process.exit();
        } else {
            console.log("DB Loaded Table: " + model);
        }
    };
}

//sets express to use swig.renderFile to render html
app.engine('html', swig.renderFile);

//sets up the html rendering engine
app.set('view engine', 'html');
app.set('views', __dirname + '/views');

//tells express to use the method-override middleware
app.use(methodOverride('_method'));

//tells express to use the bodyParser middleware
//app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//tells express static content is served from views/static
app.use(express.static(path.join(__dirname, 'views/static')));

//middleware telling express to use this orm database
app.use(orm.express("sqlite://seekr.sqlite3", {

    define: function (db, models, next) {

        //loading the models from their respective javascript classes
        db.load('./models/location',          db_error_reporter('location'));
        db.load('./models/pricepaidlocation', db_error_reporter('pricepaidlocation'));
        db.load('./models/postcode',          db_error_reporter('postcode'));

        //db.settings.set('instance.cache', false);

        //assigning the models in their express variables
        models.homemessages     = db.models.homeMessages;
        models.pplocation       = db.models.pplocation;
        models.postcode        = db.models.postcode;

        //syncing the database to create it if it doesnt already exist after 
        db.sync(function (err) {
            if (err) {
                console.log('DB Sync: Failed');
                console.log(err);
            } else {
                           //TODO do some stuff if the table already exists synchronously (waterfall method of the asynch library)
                /*var pp_input = fs.createReadStream("./brighton-500.csv");
                var pp_parser = parse({delimiter: ','});
                var pp_transformer = transform(function (record) {
                    console.log(record);
                    address = record.slice(7, 13);
                    address.push(record[3]);
                    address_str = address.join(' ');
                    models.pplocation.create([{ price: record[1], 
                                                date: record[2], 
                                                address: address_str,
                                                postcode: record[3], 
                                                longitude: 0, //currently not in use
                                                latitude: 0}], //curently not in use
                                                function(err, items) { 
                                                    if (err) {console.log("error:" + err.message);}
                                                }
                    );
                });

                pp_input.pipe(pp_parser).pipe(pp_transformer);

                var postcode_parser = parse({delimiter: ','})
                var postcode_input = fs.createReadStream('./postcodes-summary.csv');
                var postcode_transformer = transform(function(record){

                    models.postcode.create([{ postcode: record[0],
                                              longitude: record[8], 
                                              latitude: record[7]}], 
                                              function(err, items) { 
                                                if (err) {console.log("error:" + err.message);}
                                              }

                    );
                });

                postcode_input.pipe(postcode_parser).pipe(postcode_transformer);
                postcode_input.on('end', function() {
                    console.log("finished postcodes");*/
                    /*db.driver.execQuery("SELECT * FROM postcode where substr(postcode, 1, 2)='BN'", function(err, data) {
                        console.log(err);
                        console.log(data.length);
                    })*/
               // });
               // pp_input.on('end', function() {
               //     console.log("finished ppdata");
                    /*db.driver.execQuery("SELECT * FROM pplocation where substr(postcode, 1, 2)='BH'", function(err, data) {
                        console.log(err);
                        console.log(data);
                    })*/
               // })

                console.log('DB Sync: Ok');
            }
            next();
        });
    }
}));

//defining all the routes for the application

//all the get routes for the application
//app.get('/',                       request_response(controllers.homepage.get));
app.get('/location', request_response(controllers.location.get));

//the post routes for the application
app.post('/location',             request_response(controllers.location.post));

//creating and launching the server
var server = app.listen(process.env.PORT || 3000, function () {

        var host = server.address().address,
            port = server.address().port;

        console.log('Location listener running at http://%s:%s', host, port);

    });

/*        var stringify = require('csv-stringify');
        stringifier = stringify();
        var parser = parse({delimiter: ','})
                var input = fs.createReadStream('./pp-2015-part1.csv');
                var transformer = transform(function(record){
                    if (record[3].substr(0,2) == "BN") {
                        return record
                    }
                    //console.log(hi);
                    location = {}
                    address = record.slice(7, 13);
                    address.push(record[3]);
                    location.address = address.join(' ');
                    location.postcode = record[3];
                    location.date = record[2];
                    location.price = record[1];
                    location.longitude = 0;
                    location.latitude = 0;
                    models.pplocation.create([{ price: location.price, 
                                                date: location.date, 
                                                address: location.address,
                                                postcode: location.postcode, 
                                                longitude: location.longitude, 
                                                latitude: location.latitude}], function(err, items) { 
                           if (err) {
                              console.log("error:" + err.message); 
                           }
                           else {
                                console.log(items[0].postcode);
                                
                                //console.log(items[0].longitude);
                                //console.log(items);
                              
                              //console.log(items.address);
                            }

                    });
                    input.pipe(parser).pipe(transformer).pipe(stringifier).pipe(fs.createWriteStream('./brighton.csv'));
                    input.on('end', function() {
                        console.log("finished BN");
                    })
*/

/*var fs = require('fs');
                var parse = require('csv-parse');
                var transform = require('stream-transform');

                var output = [];
                var parser = parse({delimiter: ','})
                var input = fs.createReadStream('./ten-pp.csv');
                var transformer = transform(function(record, callback){
                  //setTimeout(function(){
                  //  callback(null, record.join(' ')+'\n');
                  //}, 500);
                    geocoder.geocode(record.slice(7, 13).join(' '), function(err, res) {
                        console.log(res);
                    });
                    console.log(record.slice(7, 13).join(' '));
                    models.pplocation.create([{ price: record[1], date: record[2], address: "123 Fake Street", longitude: 0, latitude: 0}], function(err, items) { 
                       if (err) {
                          console.log("error"); 
                       }
                       else {

                          console.log(items[0].address);
                        }

                    });
                }, {parallel: 10});
                input.pipe(parser).pipe(transformer);
                //.pipe(process.stdout);*/

//TODO: Error from API timeout with geocoder, only launch the server after the data has been added to the db, return data on the post call
//https://developer.appcelerator.com/question/149231/find-out-location-in-10-km-radius-from-user-current-location
//http://www.mullie.eu/geographic-searches/
//http://stackoverflow.com/questions/10318002/20-km-miles-around-a-lat-long
//https://www.gov.uk/government/statistical-data-sets/price-paid-data-downloads#single-file
//save the db somewhere so you dont have to remake it every time
//use raw sql queries to get longitudes within the square defined by the distance given by the app location

                    /*async.waterfall([
                        function(callback) {
                            location = {}
                            address = record.slice(7, 13);
                            address.push(record[3]);
                            location.address = address.join(' ');
                            location.date = record[2];
                            location.price = record[1];
                            callback(null, location);
                        },
                        function(location, callback) {
                            geocoder.geocode(location.address, function(err, res) {
                                if(res) {
                                    //exceeded the limit of API calls for google maps causes an error
                                    console.log(res);
                                    //if(!res[0].longitude){console.log(res[0])}
                                    location.longitude = res[0].longitude;
                                    location.latitude = res[0].latitude;
                                    callback(null, location);
                                }
                                else {
                                    location.longitude = 0;
                                    location.latitude = 0;
                                    callback(null, location);
                                }
                            });
                        },
                    ], function (err, location) {
                        models.pplocation.create([{ price: location.price, date: location.date, address: location.address, longitude: location.longitude, latitude: location.latitude}], function(err, items) { 
                           if (err) {
                              console.log("error:" + err.message); 
                           }
                           else {
                                
                                console.log(items[0].longitude);
                                //console.log(items);
                              
                              //console.log(items.address);
                            }

                        });
                    });*/
