var orm = require('orm');

function error_classifier(res, err) {
    if (err.code === orm.ErrorCodes.NOT_FOUND) {
        res.status(404).send("Not found");
    } else if (err.code === undefined) {
        res.status(400).send("Bad Request");
    } else {
        res.status(500).send("Internal Server Error");
    }
}

var return_types = {
    HTML : 1,
    JSON : 2
};

module.exports = {
    error_classifier : error_classifier,
    return_types     : return_types
};