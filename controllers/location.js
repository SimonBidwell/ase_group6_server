var helpers = require('./helpers');
var error_classifier = helpers.error_classifier;
var fs             = require('fs');
var parse          = require('csv-parse');
var transform      = require('stream-transform');

function create_home_message(req, res, return_type) {
    console.log("in the create controller");
    req.models.homemessages.create([{ deviceID: req.body["deviceID"], longitude: req.body["longitude"], latitude: req.body["latitude"]}], function(err, items) { 
       if (err) {
          console.log("error");
         error_classifier(res, err)   
       }
       else {
          //switch (return_type){
            //res.status(200);
            res.json(items[0]);
            //case helpers.return_types.HTML:
            //  res.redirect('/questions/' + items[0].id);
            //  break;
            //case helpers.return_types.JSON:
            //  res.status(200);
            //  res.json(items[0]);
            //  break;
          //  }
        }

    });
}

Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};

function get_home_message(req, res, return_type) {
  //defaults of 10km radius and 1 st margarets place brighton if no parameter given
  var radius = 10
  var latitude = 50.822693;
  var longitude = -0.148215;
  if(req.query.lat && req.query.long && req.query.radius) {
    radius = req.query.radius;
    latitude = req.query.lat;
    longitude = req.query.long;
  }
  /*
  var return_data = []
  var pp_input = fs.createReadStream("./output.csv");
  var pp_parser = parse({delimiter: ','});
  var pp_transformer = transform(function (record) {
                    location = {}
                    location.address = record[0];
                    location.postcode = record[1];
                    location.price = record[2];
                    location.date = record[3];
                    location.latitude = record[4]; 
                    location.longitude = record[5];
                    return_data.push(location);
                });
  pp_input.on('end', function() {
    console.log("yo");
    res.json(return_data);
  });
  pp_input.pipe(pp_parser).pipe(pp_transformer);*/

  //1km = 0.009... degrees in lat at lat 40
  var lat_displacement = 0.009006575*radius
  //1km = 0.01.. degrees in long at lat 40
  var long_displacement =0.011710973*radius
  req.db.driver.execQuery("select * from pplocation where latitude > ? and latitude < ? and longitude > ? and longitude < ?", [latitude - lat_displacement, latitude + lat_displacement, longitude - long_displacement, longitude + long_displacement],
    function (err, hms) {
  //req.models.pplocation.find().all(function (err, hms) {
        console.log(hms.length);
        if (err) {
            error_classifier(res, err);
        } else { 
            //res.status(200);
            switch (return_type) {
            case helpers.return_types.HTML:
                res.json(hms);
                break;
            case helpers.return_types.JSON:
                
                res.json(hms);
                break;
            }
        }
    });
}

module.exports = {

    post : {
       html: function (req, res) {
              create_home_message(req, res, helpers.return_types.HTML);
          },

       json: function (req, res) {
              create_home_message(req, res, helpers.return_types.JSON);
          }
    },

    get : {
      html: function (req, res) {
              get_home_message(req, res, helpers.return_types.HTML);
          },

       json: function (req, res) {
              get_home_message(req, res, helpers.return_types.JSON);
          }
    } 

}

