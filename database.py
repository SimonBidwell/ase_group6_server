import csv
import sqlite3
import time

def chunks(reader, chunksize=100):
    """ 
    Chunk generator. Take a CSV `reader` and yield
    `chunksize` sized slices. 
    """
    chunk = []
    for i, line in enumerate(reader):
        if (i % chunksize == 0 and i > 0):
            yield chunk
            del chunk[:]
        chunk.append(line)
    yield chunk

t = time.time()

conn = sqlite3.connect("seekr.sqlite3")
conn.isolation_level = None
c = conn.cursor()

csvData = csv.reader(open('processed.csv', "rb"))
divData = chunks(csvData) # divide into 10000 rows each
for chunk in divData:
    c.execute('BEGIN TRANSACTION')
    print "hi"

    for address, postcode, price, date, latitude, longitude in chunk:
        c.execute('INSERT OR IGNORE INTO pplocation (price, date, address, postcode, latitude, longitude) VALUES (?,?,?,?,?,?)', (price, date, address, postcode, latitude, longitude))
    c.execute('COMMIT')
    print "End of chunk"

print "\n Time Taken: %.3f sec" % (time.time()-t) 

# Save (commit) the changes
conn.commit()