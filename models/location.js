var orm = require('orm');

module.exports = function (db, cb) {

    var homeMessage = db.define('homeMessages', {
            deviceID     : {type: 'text', required: true},
            latitude     : {type: 'number', required: true},
            longitude    : {type: 'number', required: true}
        },
            {
                cache: false,
                autoFetchLimit: 3,
                methods: {
                    serialize: function () {
                        return {
                            id       : this.id,
                            deviceID : this.deviceID,
                            latitude : this.latitude,
                            longitude: this.longitude
                        };
                    }
                }
            });

    return cb();
};
