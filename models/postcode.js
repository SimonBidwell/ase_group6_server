var orm = require('orm');

module.exports = function (db, cb) {

    var postcode = db.define('postcode', {
            postcode     : {type: 'text', required: true},
            latitude     : {type: 'number', required: true},
            longitude    : {type: 'number', required: true}
        },
            {
                cache: false,
                autoFetchLimit: 3,
                methods: {
                    serialize: function () {
                        return {
                            id       : this.id,
                            postcode : this.postcode,
                            latitude : this.latitude,
                            longitude: this.longitude
                        };
                    }
                }
            });

    return cb();
};
