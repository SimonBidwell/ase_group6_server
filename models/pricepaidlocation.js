var orm = require('orm');

module.exports = function (db, cb) {

    var pplocation = db.define('pplocation', {
            price        : {type: 'number', required: true},
            date         : {type: 'date',   required: true},
            address      : {type: 'text',   required: true},
            postcode     : {type: 'text',   required: true},
            latitude     : {type: 'number', required: true},
            longitude    : {type: 'number', required: true}
        },
            {
                cache: false,
                autoFetchLimit: 3,
                methods: {
                    serialize: function () {
                        return {
                            id       : this.id,
                            price    : this.price,
                            date     : this.date,
                            address  : this.address,
                            postcode : this.postcode,
                            latitude : this.latitude,
                            longitude: this.longitude
                        };
                    }
                }
            });

    return cb();
};
